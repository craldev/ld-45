﻿using DUCK.Tween;
using DUCK.Tween.Easings;
using DUCK.Tween.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuLevel : MonoBehaviour
{
	[SerializeField]
	private float duration;

	[SerializeField]
	private Transform PingPongPos;

	private MoveAnimation moveDoorAnimation;

	[SerializeField]
	private AnimationCurve curve;

	private void Start()
	{
		moveDoorAnimation?.Abort();
		moveDoorAnimation = transform.MoveLocal(transform.localPosition, PingPongPos.transform.position, duration, curve.Evaluate);
		moveDoorAnimation.Play(-1);
	}
}
