﻿using LD45.Game.Gameplay;
using System.Collections;
using System.Collections.Generic;
using LD45.Game.Audio;
using UnityEngine;

public class ManualDoor : MonoBehaviour
{
	[SerializeField]
	private Collider2D Collider;

	[SerializeField]
	private Sprite ClosedSprite;
	[SerializeField]
	private Sprite OpenSprite;

	[SerializeField]
	private SpriteRenderer SpriteR;

	[SerializeField]
	private AudioClip creak;

	private bool open = false;
	private void Update()
	{
		SpriteR.sprite = open ? OpenSprite : ClosedSprite;
		Collider.enabled = !open;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("PossessedCharacter") && KeyItemManager.Value(KeyFlag.Keys))
		{
			Open();
		}
	}

	private void Open()
	{
		AudioSystem.Instance.PlaySound(creak);
		open = true;
	}
}
