using UnityEngine;

namespace LD45.Game.Character
{
	public class CharacterDefinition : MonoBehaviour
	{
		[SerializeField]
		private CharacterData data;
		public CharacterData Data => data;

		[SerializeField]
		private Transform cameraTalkLocation;
		public Transform CameraTalkLocation => cameraTalkLocation;
	}
}