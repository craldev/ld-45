using System;
using UnityEngine;

namespace LD45.Game.Character
{
	public enum CharacterName
	{
		Null,
		Goopster,
		BigBrenda,
		Steve,
		Stephen,
		Lou,
		Gilbert,
		Carol,
		Jen,
		Dexter,
		Boss,
		Rover,
		Stefan
	}

	public static class CharacterNameExt
	{
		public static bool BrendaGlasses;
		public static bool RoverCoffee;

		public static string ToFriendlyString(this CharacterName me)
		{
			switch (me)
			{
				case CharacterName.BigBrenda:
					return "Big Brenda";
			}
			return me.ToString();
		}

		public static string GetPortraitName(this CharacterName me)
		{
			switch (me)
			{
				case CharacterName.Rover when RoverCoffee:
					return "BrokenRover";
				case CharacterName.BigBrenda when BrendaGlasses:
					return "GlassesBigBrenda";
				default:
					return me.ToString();
			}
		}

		public static AudioClip GetAudio(this CharacterName me)
		{
			switch (me)
			{
				case CharacterName.Null:
					break;
				case CharacterName.BigBrenda:
				case CharacterName.Rover:
					return Resources.Load<AudioClip>("Robot_Talk");
				case CharacterName.Steve:
				case CharacterName.Stephen:
				case CharacterName.Lou:
				case CharacterName.Gilbert:
				case CharacterName.Dexter:
				case CharacterName.Stefan:
					return Resources.Load<AudioClip>("Male_Talk");
				case CharacterName.Carol:
				case CharacterName.Jen:
					return Resources.Load<AudioClip>("Female_Talk");
				case CharacterName.Boss:
					return Resources.Load<AudioClip>("Boss_Talk");
				default:
					throw new ArgumentOutOfRangeException(nameof(me), me, null);
			}

			return null;
		}

	}

	public enum CharacterType
	{
		Null,
		Ghost,
		Janitor,
		SecurityGuard,
		WorkerMale,
		WorkerFemale,
	}

	[Serializable]
	public class CharacterData
	{
		[SerializeField]
		private CharacterName character;
		public CharacterName Character { get => character; set => character = value; }

		[SerializeField]
		private CharacterType characterType;
		public CharacterType CharacterType => characterType;
	}
}