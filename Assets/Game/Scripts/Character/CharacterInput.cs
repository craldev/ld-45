﻿using System;
using LD45.Game.Event;
using UnityEngine;

namespace LD45.Game.Character
{
	public class CharacterInput : MonoBehaviour
	{
		[SerializeField]
		private bool startPlayerControlled;

		private bool isPlayerControlled;
		public bool IsPlayerControlled
		{
			get => !EventSystem.IsActive && isPlayerControlled;
			set
			{
				isPlayerControlled = value;
				OnPlayerControlledSwitch?.Invoke(value);
			}
		}

		public Vector2 MovementValue { get; private set; }

		public event Action OnCharacterJump;
		public event Action<bool> OnPlayerControlledSwitch;

		private void Awake()
		{
			IsPlayerControlled = startPlayerControlled;
		}

		private void Update()
		{
			if (IsPlayerControlled)
			{
				if (Input.GetButtonDown("Jump"))
				{
					OnCharacterJump?.Invoke();
				}
			}

			MovementValue = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		}
	}
}
