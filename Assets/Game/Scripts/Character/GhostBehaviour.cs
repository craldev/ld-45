using System;
using LD45.Game.Audio;
using LD45.Game.Event;
using UnityEngine;

namespace LD45.Game.Character
{
	public class GhostBehaviour : MonoBehaviour
	{
		[SerializeField]
		private CharacterInput input;

		[SerializeField]
		private Rigidbody2D targetLocation;

		[SerializeField]
		private AudioClip possess;

		public event Action<GameObject> OnPossess;
		public event Action<GameObject> OnUnPossess;

		private bool isPossessing;
		private bool isPossessed;
		private InteractionHandler possessedTarget;

		public void Update()
		{
			if (EventSystem.IsActive) return;
			if (Input.GetButtonDown("Possess") && possessedTarget != null)
			{
				UnPossessTarget(null);
			}
		}

		public void PossessTarget(InteractionHandler possessable)
		{
			if (isPossessing || possessedTarget == possessable) return;
			if (possessedTarget != null)
			{
				UnPossessTarget();
			}
			else
			{
				Possess();
			}

			async void Possess()
			{
				isPossessing = true;
				await Awaiters.Seconds(0.1f);
				AudioSystem.Instance.PlaySound(possess);
				isPossessing = false;
				isPossessed = true;
				possessedTarget = possessable;
				input.IsPlayerControlled = false;
				targetLocation.isKinematic = true;
				var possessableBehaviour = possessable.GetComponentInParent<PossessableBehaviour>();
				targetLocation.transform.SetParent(possessableBehaviour.StandLocation);
				targetLocation.transform.localPosition = Vector3.zero;
				possessableBehaviour.GetPossessed();
				OnPossess?.Invoke(possessable.gameObject);
			}
		}

		public async void UnPossessTarget(Action onComplete = null)
		{
			if (possessedTarget == null)
			{
				onComplete?.Invoke();
				return;
			}

			isPossessing = true;
			await Awaiters.Seconds(0.1f);
			isPossessing = false;
			isPossessed = false;
			if (possessedTarget != null)
			{
				var possessableBehaviour = possessedTarget.GetComponentInParent<PossessableBehaviour>();
				possessableBehaviour.RegainControl();
			}

			OnUnPossess?.Invoke(gameObject);
			input.IsPlayerControlled = true;
			targetLocation.isKinematic = false;
			targetLocation.transform.SetParent(transform);
			possessedTarget = null;
			onComplete?.Invoke();

		}
	}
}