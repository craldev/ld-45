using System.Collections.Generic;
using System.Linq;
using LD45.Game.Event;
using LD45.Game.Gameplay;
using LD45.Game.Menu;
using UnityEngine;

namespace LD45.Game.Character
{
	public class InteractionTrigger : MonoBehaviour
	{
		[SerializeField]
		private CharacterInput input;

		[SerializeField]
		private GhostBehaviour behaviour;

		[SerializeField]
		private InteractionHandler interactionHandler;

		[SerializeField]
		private List<InteractionHandler> actors = new List<InteractionHandler>();

		[SerializeField]
		private List<PickableItem> items = new List<PickableItem>();

		private void Awake()
		{
			EventSystem.OnEventStarted += ClearAll;
			if (input != null)
			{
				input.OnPlayerControlledSwitch += HandlePlayerControlledSwitch;
			}
		}

		private void OnTriggerEnter2D(Collider2D other)
		{
			var interaction = other.GetComponentInChildren<InteractionHandler>();
			if (interaction != null)
			{
				if (!actors.Contains(interaction))
				{
					actors.Add(interaction);
				}
			}
			var item = other.GetComponentInChildren<PickableItem>();
			if (item != null)
			{
				if (!items.Contains(item))
				{
					items.Add(item);
				}
			}
		}

		private void OnTriggerExit2D(Collider2D other)
		{
			var interaction = other.GetComponentInChildren<InteractionHandler>();
			if (interaction != null)
			{
				if (actors.Contains(interaction))
				{
					actors.Remove(interaction);
					interaction.ResetFlip();
					interaction.Clear();
				}
			}

			var item = other.GetComponentInChildren<PickableItem>();
			if (item != null)
			{
				if (items.Contains(item))
				{
					items.Remove(item);
					item.ClearPrompt();
				}
			}
		}

		private void Update()
		{
			if (EventSystem.IsActive || !interactionHandler.IsPlayerControlled || LoadingScreen.IsActive)
			{
				return;
			}

			if (interactionHandler.Definition.Data.CharacterType != CharacterType.Ghost)
			{
				foreach (var item in items)
				{
					if (item.CanInteract(interactionHandler.Definition.Data.Character))
					{
						item.Activate(interactionHandler);
						foreach (var actor in actors)
						{
							actor.Clear();
						}

						return;
					}
				}
			}

			actors = actors.OrderBy(actor => Vector3.Distance(transform.position, actor.transform.position)).ToList();
			var activeInteractive = false;
			foreach (var actor in actors)
			{
				if (!actor.IsPlayerControlled && !activeInteractive)
				{
					if (interactionHandler.Definition.Data.CharacterType == CharacterType.Ghost)
					{
						if (actor.Definition.Data.CharacterType != CharacterType.Null)
						{
							actor.DisplayPossessable(behaviour);
							activeInteractive = true;
							continue;
						}
					}

					if (interactionHandler.Definition.Data.CharacterType != CharacterType.Null && interactionHandler.Definition.Data.CharacterType != CharacterType.Ghost)
					{
						actor.FlipX(interactionHandler.transform.position.x <= actor.transform.position.x);
						actor.DisplayChat(interactionHandler);
						activeInteractive = true;
					}
				}
				else
				{
					actor.Clear();
				}
			}
		}


		private void HandlePlayerControlledSwitch(bool value)
		{
			if (!value)
			{
				ClearAll();
			}
		}


		private void ClearAll()
		{
			interactionHandler.Clear();

			foreach (var actor in actors)
			{
				actor.Clear();
			}

			foreach (var item in items)
			{
				item.ClearPrompt();
			}
		}

	}
}