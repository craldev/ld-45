﻿using LD45.Game.Shader;
using UnityEngine;

namespace LD45.Game.Character
{
	public class PossessableBehaviour : MonoBehaviour
	{
		[SerializeField]
		private CharacterInput input;

		[SerializeField]
		private SpriteOutline outline;

		[SerializeField]
		private Transform standLocation;
		public Transform StandLocation => standLocation;

		public void GetPossessed()
		{
			gameObject.layer = LayerMask.NameToLayer("PossessedCharacter");
			outline.outlineSize = 1;
			input.IsPlayerControlled = true;
		}

		public void RegainControl()
		{
			gameObject.layer = LayerMask.NameToLayer("Character");
			outline.outlineSize = 0;
			input.IsPlayerControlled = false;
		}
	}
}