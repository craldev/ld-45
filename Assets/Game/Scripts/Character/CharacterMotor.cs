﻿using System;
using UnityEngine;

namespace LD45.Game.Character
{
	public class CharacterMotor : MonoBehaviour
	{
		[SerializeField]
		private new Rigidbody2D rigidbody;

		[SerializeField]
		private CharacterInput input;

		[SerializeField]
		private float movementSpeed = 10f;

		[SerializeField]
		private float jumpForce = 10f;

		[SerializeField]
		private float gravityAcceleration = 3f;

		[SerializeField]
		private float gravityForce = 20f;

		[SerializeField]
		private LayerMask groundedLayerMask;

		private bool isGrounded;
		private bool hasJumped;
		private float currentGravityForce;

		public event Action<bool> OnGroundedChanged;

		private void Awake()
		{
			input.OnCharacterJump += HandleJump;
			rigidbody.gravityScale = 0f;
		}

		private void Update()
		{
			var hit = Physics2D.Raycast(transform.position + Vector3.up, -Vector2.up, 1.1f, groundedLayerMask);
			var prevGroundedState = isGrounded;
			isGrounded = hit.collider != null;

			if (isGrounded != prevGroundedState)
			{
				OnGroundedChanged?.Invoke(isGrounded);
			}

			if (isGrounded && rigidbody.velocity.y < 0)
			{
				hasJumped = false;
				currentGravityForce -= Time.deltaTime * gravityAcceleration;
				currentGravityForce = Mathf.Clamp(currentGravityForce, 0, gravityForce);
			}
			else if (!isGrounded)
			{
				currentGravityForce += Time.deltaTime * gravityAcceleration;
				currentGravityForce = Mathf.Clamp(currentGravityForce, 0, gravityForce);
			}
		}

		private void FixedUpdate()
		{
			var targetVelocity = Vector3.zero;

			targetVelocity.y = rigidbody.velocity.y - currentGravityForce;
			if (input.IsPlayerControlled)
			{
				targetVelocity.x = input.MovementValue.x * movementSpeed;
			}

			rigidbody.velocity = targetVelocity;
		}

		private void HandleJump()
		{
			if (!isGrounded && hasJumped) return;
			hasJumped = true;
			rigidbody.velocity = Vector3.zero;
			currentGravityForce = 0f;
			rigidbody.AddForce(Vector3.up * jumpForce);
		}
	}
}