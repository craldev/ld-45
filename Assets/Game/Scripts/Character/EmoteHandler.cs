using UnityEngine;

namespace LD45.Game.Character
{
	public class EmoteHandler : MonoBehaviour
	{
		[SerializeField]
		private SpriteRenderer chatIcon;

		[SerializeField]
		private SpriteRenderer fbuttonIcon;

		public void ShowChat()
		{
			if (chatIcon != null)
			{
				chatIcon.enabled = true;
			}
		}

		public void HideChat()
		{
			if (chatIcon != null)
			{
				chatIcon.enabled = false;
			}
		}

		public void ShowPossess()
		{
			if (fbuttonIcon != null)
			{
				fbuttonIcon.enabled = true;
			}
		}

		public void HidePossess()
		{
			if (fbuttonIcon != null)
			{
				fbuttonIcon.enabled = false;
			}
		}
	}
}