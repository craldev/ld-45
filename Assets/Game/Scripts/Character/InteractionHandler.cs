using LD45.Game.Event;
using UnityEngine;

namespace LD45.Game.Character
{
	public class InteractionHandler : MonoBehaviour
	{
		[SerializeField]
		private EventManager eventManager;

		[SerializeField]
		private CharacterInput input;

		[SerializeField]
		private EmoteHandler emoteHandler;

		[SerializeField]
		private CharacterAnimator characterAnimator;

		[SerializeField]
		private CharacterDefinition definition;
		public CharacterDefinition Definition => definition;

		private bool isPossessable;
		private bool isInteractable = false;
		private GhostBehaviour ghost;
		private InteractionHandler initiator;
		private bool startingFlip;

		public bool IsPlayerControlled => (input != null && input.IsPlayerControlled) || EventSystem.IsActive;

		private void Awake()
		{
			Clear();
		}

		public void Clear()
		{
			emoteHandler.HideChat();
			emoteHandler.HidePossess();
			ghost = null;
			initiator = null;
			isPossessable = false;
			isInteractable = false;
		}

		public void DisplayPossessable(GhostBehaviour ghostBehaviour)
		{
			ghost = ghostBehaviour;
			emoteHandler.ShowPossess();
			isPossessable = true;
		}

		public void DisplayChat(InteractionHandler initiator)
		{
			if (Definition.Data.CharacterType == CharacterType.Ghost) return;
			this.initiator = initiator;
			emoteHandler.ShowChat();
			isInteractable = true;
		}

		private void Update()
		{
			if (EventSystem.IsActive) return;
			if (isPossessable && Input.GetButtonDown("Possess"))
			{
				ghost.PossessTarget(this);
			}
			else if (Definition.Data.CharacterType != CharacterType.Ghost && isInteractable && Input.GetButtonDown("Use"))
			{
				eventManager.PlayEvent(initiator);
			}
		}

		public void FlipX(bool value)
		{
			characterAnimator.FlipX(value);
		}

		public void ResetFlip()
		{
			characterAnimator.ResetFlip();
		}
	}
}