using System.Collections.Generic;
using UnityEngine;

namespace LD45.Game.Character
{
	public class CharacterAnimator : MonoBehaviour
	{
		[SerializeField]
		private Animator animator;

		[SerializeField]
		private Animator seperateAnimator;

		[SerializeField]
		private CharacterInput input;

		[SerializeField]
		private CharacterMotor motor;

		[SerializeField]
		private List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();

		[SerializeField]
		private List<Transform> flipTransforms;

		[SerializeField]
		private new Rigidbody2D rigidbody;

		[SerializeField]
		private bool startingFlip;

		private Dictionary<Vector2, Transform> flipReference = new Dictionary<Vector2, Transform>();
		private static readonly int offset = Animator.StringToHash("Offset");

		private void Awake()
		{
			var offsetValue = Random.Range(0f, 1f);
			animator.SetFloat(offset, offsetValue);
			if (seperateAnimator != null)
			{
				seperateAnimator.SetFloat(offset, offsetValue);
			}

			FlipX(startingFlip);
			if (motor != null)
			{
				motor.OnGroundedChanged += HandleMovementAnimation;
			}

			foreach (var flip in flipTransforms)
			{
				flipReference.Add(flip.localPosition, flip);
			}
		}

		private void Update()
		{
			if (rigidbody.velocity.x > 0 && input.MovementValue.x > 0)
			{
				FlipX(false);

			}
			else if (rigidbody.velocity.x < 0 && input.MovementValue.x < 0)
			{
				FlipX(true);

			}
		}

		public void FlipX(bool value)
		{
			foreach (var spriteRenderer in spriteRenderers)
			{
				spriteRenderer.flipX = value;
			}

			foreach (var flips in flipReference)
			{
				if (value)
				{
					flips.Value.localPosition = new Vector2(-flips.Key.x, flips.Value.localPosition.y);
				}
				else
				{
					flips.Value.localPosition = new Vector2(flips.Key.x, flips.Value.localPosition.y);
				}
			}
		}

		public void ResetFlip()
		{
			foreach (var spriteRenderer in spriteRenderers)
			{
				spriteRenderer.flipX = startingFlip;
			}
		}

		private void HandleMovementAnimation(bool groundedState)
		{
			if (groundedState)
			{
				animator.Play("Movement");
			}
		}

	}
}