﻿using System;
using LD45.Game.Audio;
using LD45.Game.Core;
using UnityEngine;

namespace LD45.Game.Character
{
	public class GameService : MonoBehaviour, ILevelHandleInitialize
	{
		[SerializeField]
		private AudioClip backgroundMusic;

		[SerializeField]
		private GhostBehaviour startingPlayer;
		public GhostBehaviour StartingPlayer => startingPlayer;

		public GameObject Ghost { get; set; }
		public GameObject CurrentPlayer { get; set; }

		public event Action onPlayerChanged;

		private void Awake()
		{
			Ghost = startingPlayer.gameObject;
			startingPlayer.OnPossess += HandleChangedPlayer;
			startingPlayer.OnUnPossess += HandleChangedPlayer;
			HandleChangedPlayer(startingPlayer.gameObject);
		}

		private void HandleChangedPlayer(GameObject player)
		{
			CurrentPlayer = player;
			onPlayerChanged?.Invoke();
		}

		public void HandleLevelInitialized()
		{
			AudioSystem.Instance.PlayMusic(backgroundMusic);
		}
	}
}