using UnityEngine;

namespace LD45.Game.Character
{
	public class GhostMotor : MonoBehaviour
	{
		[SerializeField]
		private new Rigidbody2D rigidbody;

		[SerializeField]
		private Rigidbody2D targetLocation;

		[SerializeField]
		private CharacterInput input;

		[SerializeField]
		private float inputSpeed;

		[SerializeField]
		private float movementSpeed;

		private void Awake()
		{
			targetLocation.position = transform.position;
			rigidbody.gravityScale = 0f;
		}

		private void Update()
		{
			if (input.IsPlayerControlled)
			{
				targetLocation.MovePosition(targetLocation.position + input.MovementValue * inputSpeed);
			}
		}

		private void FixedUpdate()
		{
			var distance = Vector3.Distance(targetLocation.position, transform.position);
			var direction = (targetLocation.position - (Vector2)transform.position).normalized;
			rigidbody.AddForce(direction * movementSpeed * distance);
		}
	}
}