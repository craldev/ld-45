﻿using System;
using DUCK.Tween;
using DUCK.Tween.Easings;
using DUCK.Tween.Extensions;
using LD45.Game.Audio;
using LD45.Game.Core;
using LD45.Game.Event;
using LD45.Game.Menu;
using UnityEngine;
using UnityEngine.UI;

namespace LD45.Game.Settings
{
	public class SettingsSystem : MonoBehaviour, ILevelHandleInitialize
	{
		[SerializeField]
		private CanvasGroup canvasGroup;

		[SerializeField]
		private Slider masterVolume;

		[SerializeField]
		private Slider musicVolume;

		[SerializeField]
		private Slider audioVolume;

		private UIFadeAnimation fadeAnimation;
		private bool isOpen = false;
		public static SettingsSystem Instance { get; private set; }

		private void Awake()
		{
			if (Instance != null)
			{
				throw new Exception("There should only be a single instance of 'SettingsSystem' active at one time.");
			}

			Instance = this;
			canvasGroup.blocksRaycasts = false;
			canvasGroup.alpha = 0f;
		}

		private void Start()
		{
			Load();
		}

		private void Update()
		{
			if (LoadingScreen.IsActive || EventSystem.IsActive) return;
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				if (isOpen)
				{
					Close();
				}
				else
				{
					Open();
				}
			}
		}

		public void Open()
		{
			isOpen = true;
			Time.timeScale = 0f;
			fadeAnimation?.Abort();
			fadeAnimation = canvasGroup.Fade(canvasGroup.alpha, 1, 0.5f, Ease.Quad.InOut);
			fadeAnimation.Play(() => { canvasGroup.blocksRaycasts = true; });
			Load();
		}

		public void Save()
		{
			PlayerPrefs.SetFloat("Music", musicVolume.value);
			PlayerPrefs.SetFloat("Audio", audioVolume.value);
			PlayerPrefs.SetFloat("Master", masterVolume.value);
		}

		public void Load()
		{
			if (PlayerPrefs.HasKey("Master"))
			{
				Debug.Log("Loading");
				musicVolume.SetValueWithoutNotify(PlayerPrefs.GetFloat("Music"));
				audioVolume.SetValueWithoutNotify(PlayerPrefs.GetFloat("Audio"));
				masterVolume.SetValueWithoutNotify(PlayerPrefs.GetFloat("Master"));
				MasterVolumeChanged();
				MusicVolumeChanged();
				AudioVolumeChanged();
			}
		}

		public void Close()
		{
			fadeAnimation?.Abort();
			fadeAnimation = canvasGroup.Fade(canvasGroup.alpha, 0, 0.5f, Ease.Quad.InOut);
			fadeAnimation.Play(() =>
			{
				canvasGroup.blocksRaycasts = false;
				Time.timeScale = 1f;
				isOpen = false;
			});
			Save();
		}

		public void Quit()
		{
			Application.Quit();
		}

		public void MasterVolumeChanged()
		{
			AudioSystem.Instance.AdjustMasterVolume(masterVolume.value);
		}

		public void MusicVolumeChanged()
		{
			AudioSystem.Instance.AdjustMusicVolume(musicVolume.value);
		}

		public void AudioVolumeChanged()
		{
			AudioSystem.Instance.AdjustAudioVolume(audioVolume.value);
		}

		public void WindowedActivate()
		{
			Screen.fullScreenMode = FullScreenMode.Windowed;
			Screen.SetResolution (1280, 720, false);
		}

		public void FullscreenActivate()
		{
			Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
			Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
		}

		public void BorderlessActivate()
		{
			Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
			Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
		}

		public void HandleLevelInitialized()
		{
			Load();
		}
	}
}