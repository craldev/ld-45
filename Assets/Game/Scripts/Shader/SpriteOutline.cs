﻿using UnityEngine;

namespace LD45.Game.Shader
{
	[ExecuteInEditMode]
	public class SpriteOutline : MonoBehaviour
	{
		public Color color = Color.white;

		[Range(-1, 16)]
		public int outlineSize = 1;

		private SpriteRenderer spriteRenderer;

		private void OnEnable()
		{
			spriteRenderer = GetComponent<SpriteRenderer>();

			UpdateOutline(true);
		}

		private void OnDisable()
		{
			UpdateOutline(false);
		}

		private void Update()
		{
			UpdateOutline(true);
		}

		private void UpdateOutline(bool outline)
		{
			var mpb = new MaterialPropertyBlock();
			spriteRenderer.GetPropertyBlock(mpb);
			mpb.SetFloat("_Outline", outline ? 1f : 0);
			mpb.SetColor("_OutlineColor", color);
			mpb.SetFloat("_OutlineSize", outlineSize);
			spriteRenderer.SetPropertyBlock(mpb);
		}
	}
}