namespace LD45.Game.Core
{
	public interface ILevelHandleInitialize
	{
		void HandleLevelInitialized();
	}
}