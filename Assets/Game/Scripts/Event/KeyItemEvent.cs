﻿using System;
using LD45.Game.Character;
using LD45.Game.Gameplay;
using UnityEngine;

namespace LD45.Game.Event
{
	public class KeyItemEvent : AbstractEvent
	{
		[SerializeField]
		private KeyFlag flag;

		public override void EventEnter(InteractionHandler initiator, Action onComplete)
		{
			KeyItemManager.AddFlag(flag);
			onComplete?.Invoke();
		}
	}
}
