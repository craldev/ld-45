using System;
using System.Collections.Generic;
using UnityEngine;

namespace LD45.Game.Event
{
	[Serializable]
	public class EventSequence : MonoBehaviour
	{
		[SerializeField]
		private bool startWithPossessed;
		public bool StartWithPossessed => startWithPossessed;

		[SerializeField]
		private Transform startingCameraLocation;
		public Transform StartingCameraLocation => startingCameraLocation;

		[SerializeField]
		private List<AbstractEvent> events = new List<AbstractEvent>();
		public List<AbstractEvent> Events => events;
	}
}