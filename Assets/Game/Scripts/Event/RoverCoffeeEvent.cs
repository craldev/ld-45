using System;
using LD45.Game.Character;

namespace LD45.Game.Event
{
	public class RoverCoffeeEvent : AbstractEvent
	{
		public override async void EventEnter(InteractionHandler initiator, Action onComplete)
		{
			CharacterNameExt.RoverCoffee = true;
			await Awaiters.Seconds(0.2f);
			onComplete?.Invoke();
		}
	}
}