﻿using System;
using System.Collections.Generic;
using LD45.Game.Character;
using LD45.Game.Gameplay;
using UnityEngine;

namespace LD45.Game.Event
{
	[Serializable]
	public class EventData
	{
		public CharacterName character;
		public List<EventSequenceData> eventSequence;

		public EventSequence GetEventSequence()
		{
			foreach (var sequence in eventSequence)
			{
				if (KeyItemManager.Value(sequence.flag))
				{
					return sequence.eventSequence;
				}
			}

			return null;
		}
	}

	[Serializable]
	public class EventSequenceData
	{
		public KeyFlag flag;
		public EventSequence eventSequence;
	}

	public class EventManager : MonoBehaviour
	{
		[SerializeField]
		private List<EventData> specificEvents = new List<EventData>();

		[SerializeField]
		private List<EventSequence> defaultEvents = new List<EventSequence>();

		private Dictionary<CharacterName, EventData> specificCharacterEvent = new Dictionary<CharacterName, EventData>();

		private void Awake()
		{
			foreach (var eventData in specificEvents)
			{
				specificCharacterEvent.Add(eventData.character, eventData);
			}
		}

		public void PlayEvent(InteractionHandler initiator)
		{
			if (specificCharacterEvent.ContainsKey(initiator.Definition.Data.Character))
			{
				var eventSequence = specificCharacterEvent[initiator.Definition.Data.Character].GetEventSequence();
				if (eventSequence != null)
				{
					EventSystem.Instance.Activate(initiator, eventSequence);
					return;
				}
			}

			EventSystem.Instance.Activate(initiator, defaultEvents[0]);

			if (defaultEvents.Count > 1)
			{
				defaultEvents.RemoveAt(0);
			}
		}
	}
}