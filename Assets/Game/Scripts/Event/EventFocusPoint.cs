using LD45.Game.Core;
using UnityEngine;

namespace LD45.Game.Event
{
	public class EventFocusPoint : MonoBehaviour, ILevelHandleInitialize
	{
		public static EventFocusPoint Instance { get; private set; }

		public void HandleLevelInitialized()
		{
			if (Instance != null) return;

			Instance = this;
			transform.SetParent(null);
			transform.position = Vector3.zero;
			DontDestroyOnLoad(this);
		}

		public void MoveToPosition(Vector3 position)
		{
			transform.position = position;
		}

		public void MoveToLocation(Transform location)
		{
			transform.position = location.position;
		}
	}
}