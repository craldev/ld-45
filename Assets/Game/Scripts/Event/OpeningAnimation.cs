﻿using DUCK.Tween.Extensions;
using LD45.Game.Audio;
using LD45.Game.Character;
using LD45.Game.Core;
using UnityEngine;

namespace LD45.Game.Event
{
	public class OpeningAnimation : MonoBehaviour, ILevelHandleInitialize
	{
		[SerializeField]
		private Animator animator;

		[SerializeField]
		private SpriteRenderer ghost;

		[SerializeField]
		private CharacterInput ghostInput;

		[SerializeField]
		private AudioClip clip;

		private void Awake()
		{
			ghost.color = new Color32(255,255,255,0);
			ghostInput.enabled = false;
		}

		public async void HandleLevelInitialized()
		{
			animator.Play("Fall");
			await Awaiters.Seconds(0.3f);
			AudioSystem.Instance.PlaySound(clip);
			await Awaiters.Seconds(1.7f);

			ghost.FadeTo(0.7f, 1f).Play(() => { ghostInput.enabled = true; });

		}
	}
}
