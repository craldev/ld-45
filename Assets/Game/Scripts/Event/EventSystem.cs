using System;
using System.Collections;
using DUCK.Tween;
using DUCK.Tween.Easings;
using DUCK.Tween.Extensions;
using LD45.Game.Audio;
using LD45.Game.Character;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LD45.Game.Event
{
	public class EventSystem : MonoBehaviour
	{
		[SerializeField]
		private CanvasGroup canvasGroup;

		[SerializeField]
		private TextMeshProUGUI characterName;

		[SerializeField]
		private TextMeshProUGUI dialogueText;

		[SerializeField]
		private Image portrait;

		private UIFadeAnimation fadeAnimation;
		private bool isActive;
		private bool isAnimating;
		private EventSequence currentEvent;
		[SerializeField]
		private int index;
		private InteractionHandler initiator;
		private bool isDisplayingText;
		private string currentText;
		private string currentString;

		public static EventSystem Instance { get; private set; }
		public static bool IsActive => Instance.isActive;
		public static event Action OnEventStarted;
		public static event Action OnEventFinished;

		private void Awake()
		{
			if (Instance != null)
			{
				throw new Exception("There should only be a single instance of 'EventSystem' active at one time.");
			}

			Instance = this;
			canvasGroup.alpha = 0f;
			ClearUI();
		}

		private void Update()
		{
			if (!isActive || isAnimating) return;
			if (Input.GetButtonDown("Jump") || Input.GetButtonDown("Use"))
			{
				ProgressEvent();
			}
		}

		public void Activate(InteractionHandler initiator, EventSequence eventSequence)
		{
			if (currentEvent != null) throw new Exception("You can only have 1 Event Sequence active at one time!");

			if (eventSequence.Events.Count < 1)
			{
				throw new Exception("Event Sequence has no events attached!");
			}

			if (eventSequence.StartWithPossessed)
			{
				EventFocusPoint.Instance.MoveToLocation(initiator.Definition.CameraTalkLocation);
			}
			else if (eventSequence.StartingCameraLocation != null)
			{
				EventFocusPoint.Instance.MoveToLocation(eventSequence.StartingCameraLocation);
			}

			this.initiator = initiator;
			OnEventStarted?.Invoke();
			index = 0;
			currentEvent = eventSequence;
			isActive = true;
			isAnimating = true;
			ClearUI();
			BlackBarSystem.Instance.AnimateIn(() =>
			{
				UpdateUI(false);
				fadeAnimation?.Abort();
				fadeAnimation = canvasGroup.Fade(canvasGroup.alpha, 1, 0.5f, Ease.Quad.InOut);
				fadeAnimation.Play(() =>
				{
					isAnimating = false;
					PlayEvent();
				});
			});
		}

		private void PlayEvent()
		{
			currentEvent.Events[index].EventEnter(initiator, ProgressEvent);
			UpdateUI(true);
		}

		private void ProgressEvent()
		{
			if (isDisplayingText)
			{
				isDisplayingText = false;
				return;
			}

			index++;
			if (index >= currentEvent.Events.Count)
			{
				EndEvent();
			}
			else
			{
				PlayEvent();
			}
		}

		private void EndEvent()
		{
			OnEventFinished?.Invoke();
			fadeAnimation?.Abort();
			fadeAnimation = canvasGroup.Fade(canvasGroup.alpha, 0, 0.5f, Ease.Quad.InOut);
			fadeAnimation.Play(() =>
			{
				BlackBarSystem.Instance.AnimateOut(() =>
				{
					currentEvent = null;
					index = 0;
					isActive = false;
					initiator = null;
				});
			});
		}

		private void UpdateUI(bool displayText)
		{
			if (index >= currentEvent.Events.Count) index--;

			switch (currentEvent.Events[index])
			{
				case DialogueEvent dialogue:
					if (displayText)
					{
						characterName.text = dialogue.character.ToFriendlyString();
						currentText = dialogue.dialogueText;
						var isItalics = false;
						if (currentText.StartsWith("<i>"))
						{
							isItalics = true;
							currentText = currentText.Substring(3, currentText.Length - 7);
						}
						StartCoroutine(PlayText(isItalics, dialogue.character.GetAudio()));
					}

					portrait.sprite = Resources.Load<Sprite>("Portraits/" + dialogue.character.GetPortraitName());
					break;
			}
		}

		private void ClearUI()
		{
			characterName.text = "";
			dialogueText.text = "";
			portrait.sprite = null;
		}

		private IEnumerator PlayText(bool italics, AudioClip clip)
		{
			isDisplayingText = true;
			dialogueText.text = "";

			for (var i = 0; i < currentText.Length; i++)
			{
				var c = currentText[i];
				if (isDisplayingText == false)
				{
					dialogueText.text = italics ? "<i>" + currentText + "</i>" : currentText;
					break;
				}

				currentString += c;

				dialogueText.text = italics ? "<i>" + currentString + "</i>" : currentString;

				if (clip != null && i % 6 == 0)
				{
					AudioSystem.Instance.PlaySound(clip);
				}

				yield return new WaitForSeconds(0.03f);
			}

			isDisplayingText = false;
			currentString = "";
		}

	}
}