using System;
using DUCK.Tween;
using DUCK.Tween.Easings;
using DUCK.Tween.Extensions;
using UnityEngine;

namespace LD45.Game.Event
{
	public class BlackBarSystem : MonoBehaviour
	{
		[SerializeField]
		private RectTransform topBar;

		[SerializeField]
		private RectTransform bottomBar;

		private ParalleledAnimation moveAnimation;
		private bool isActive;

		public static BlackBarSystem Instance { get; private set; }

		private void Awake()
		{
			if (Instance != null)
			{
				throw new Exception("There should only be a single instance of 'BlackBarSystem' active at one time.");
			}

			Instance = this;
			Deactivate();
		}

		public void Activate()
		{
			topBar.localPosition = Vector3.zero;
			bottomBar.localPosition = Vector3.zero;
			isActive = true;
		}

		public void Deactivate()
		{
			topBar.anchoredPosition3D = new Vector3(0f, 200f, 0f);
			bottomBar.anchoredPosition3D = new Vector3(0f, -200f, 0f);
			isActive = false;
		}

		public void AnimateIn(Action onComplete = null, float duration = 1f)
		{
			if (isActive) return;

			moveAnimation?.Abort();
			moveAnimation = new ParalleledAnimation();
			moveAnimation.Add(topBar.MoveAnchor(topBar.anchoredPosition3D, Vector3.zero, duration, Ease.Quad.InOut));
			moveAnimation.Add(bottomBar.MoveAnchor(bottomBar.anchoredPosition3D, Vector3.zero, duration, Ease.Quad.InOut));
			moveAnimation.Play(onComplete);
			isActive = true;
		}

		public void AnimateOut(Action onComplete = null, float duration = 1f)
		{
			moveAnimation?.Abort();
			moveAnimation = new ParalleledAnimation();
			moveAnimation.Add(topBar.MoveAnchor(topBar.anchoredPosition3D, new Vector3(0f, 200f, 0f), duration, Ease.Quad.InOut));
			moveAnimation.Add(bottomBar.MoveAnchor(bottomBar.anchoredPosition3D, new Vector3(0f, -200f, 0f), duration, Ease.Quad.InOut));
			moveAnimation.Play(onComplete);
			isActive = false;
		}
	}
}