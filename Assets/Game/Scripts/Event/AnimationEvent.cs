using System;
using LD45.Game.Character;
using UnityEngine;

namespace LD45.Game.Event
{
	public class AnimationEvent : AbstractEvent
	{
		[SerializeField]
		private Animator animator;

		[SerializeField]
		private float initialDelay = 0.3f;

		[SerializeField]
		private float afterDelay = 0.3f;

		[SerializeField]
		private string destinationState;

		public override async void EventEnter(InteractionHandler initiator, Action onComplete)
		{
			await Awaiters.Seconds(initialDelay);
			animator.Play(destinationState);
			await Awaiters.Seconds(afterDelay);
			onComplete?.Invoke();
		}
	}
}