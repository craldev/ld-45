using System;
using LD45.Game.Character;
using UnityEngine;

namespace LD45.Game.Event
{
	[Serializable]
	public class DialogueEvent : AbstractEvent
	{
		[SerializeField]
		private bool usePossessedTalkCamera;

		[SerializeField]
		private Transform cameraPosition;

		public CharacterName character;

		public string dialogueText;

		public override void EventEnter(InteractionHandler initiator, Action onComplete)
		{
			if (usePossessedTalkCamera)
			{
				EventFocusPoint.Instance.MoveToLocation(initiator.Definition.CameraTalkLocation);
			}
			else
			{
				if (cameraPosition != null)
				{
					EventFocusPoint.Instance.MoveToLocation(cameraPosition);
				}
			}
		}
	}
}