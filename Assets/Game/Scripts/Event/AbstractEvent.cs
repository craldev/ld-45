using System;
using LD45.Game.Character;
using UnityEngine;

namespace LD45.Game.Event
{
	[Serializable]
	public abstract class AbstractEvent : MonoBehaviour
	{
		public abstract void EventEnter(InteractionHandler initiator, Action onComplete);
	}
}