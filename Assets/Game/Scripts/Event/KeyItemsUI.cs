﻿using DUCK.Tween.Extensions;
using LD45.Game.Audio;
using LD45.Game.Core;
using LD45.Game.Gameplay;
using UnityEngine;
using UnityEngine.UI;

namespace LD45.Game.Event
{
	public class KeyItemsUI : MonoBehaviour, ILevelHandleInitialize
	{
		[SerializeField]
		private CanvasGroup canvas;

		[SerializeField]
		private Image glasses;

		[SerializeField]
		private Image pass;

		[SerializeField]
		private Image coffee;

		[SerializeField]
		private Image keys;

		[SerializeField]
		private Image hammer;

		[SerializeField]
		private AudioClip unlock;

		private void Awake()
		{
			canvas.alpha = 0f;
			KeyItemManager.OnCollectedFlag += HandleKeyItem;
		}

		private void HandleKeyItem(KeyFlag flag)
		{
			Image image;
			switch (flag)
			{
				case KeyFlag.Glasses:
					image = glasses;
					break;
				case KeyFlag.Pass:
					image = pass;
					break;
				case KeyFlag.Coffee:
					image = coffee;
					break;
				case KeyFlag.Keys:
					image = keys;
					break;
				case KeyFlag.Hammer:
					image = hammer;
					break;
				default:
					return;
			}

			AudioSystem.Instance.PlaySound(unlock);
			image.ColorFadeTo(KeyItemManager.Value(flag) ? Color.white : Color.black, 0.5f).Play();
		}

		public void HandleLevelInitialized()
		{
			canvas.alpha = 1f;
		}
	}
}