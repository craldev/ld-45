using System;
using LD45.Game.Character;

namespace LD45.Game.Event
{
	public class BrendaGlassesEvent : AbstractEvent
	{
		public override async void EventEnter(InteractionHandler initiator, Action onComplete)
		{
			CharacterNameExt.BrendaGlasses = true;
			await Awaiters.Seconds(0.2f);
			onComplete?.Invoke();
		}
	}
}