﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace LD45.Game.Utils.Editor
{
	[InitializeOnLoad]
	public class Physics2DSimulate
	{
		private static readonly bool registered;

		private static bool active;
		private static Rigidbody2D[] workList;
		private static Vector3[] startingPositions;
		private static Quaternion[] startingRotations;
		private static bool cachedAutoSimulation;
		private static float activeTime;

		private const float TIME_TO_SETTLE = 2f;

		static Physics2DSimulate()
		{
			if (!registered)
			{
				// hook into the editor update
				EditorApplication.update += Update;
				registered = true;
			}
		}

		[MenuItem("Tools/Physic Simulation/Gravity Simulation")]
		public static void SimulateWithGravity()
		{
			Simulate(FindSelection());
		}

		private static Rigidbody2D[] FindSelection()
		{
			var selection = Selection.transforms.ToList();
			var rigidbody2DSelection = selection.Select(transform => transform.GetComponent<Rigidbody2D>())
				.Where(rigidbody2D => rigidbody2D != null).ToList();

			foreach (var select in selection)
			{
				rigidbody2DSelection.AddRange(select.GetComponentsInChildren<Rigidbody2D>());
			}

			return rigidbody2DSelection.Distinct().ToArray();
		}

		private static void Simulate(Rigidbody2D[] selection)
		{
			if (!active)
			{
				active = true;

				workList = selection ?? Object.FindObjectsOfType<Rigidbody2D>();

				cachedAutoSimulation = Physics2D.autoSimulation;
				activeTime = 0f;

				foreach (var body in workList)
				{
					body.isKinematic = false;
				}

				foreach (var body in workList)
				{
					Undo.RecordObject(body,"Simulated physics on " + body.name);
					body.WakeUp();
				}
			}
		}

		private static void Update()
		{
			if (active)
			{
				activeTime += Time.deltaTime;
				Physics2D.autoSimulation = false;

				if (activeTime >= TIME_TO_SETTLE)
				{
					Physics2D.autoSimulation = cachedAutoSimulation;
					active = false;

					foreach (var body in workList)
					{
						body.transform.position = body.position;
						body.transform.rotation = Quaternion.Euler(new Vector3(0, 0 ,body.rotation));
					}
				}
				else
				{
					Physics2D.Simulate(Time.deltaTime);
				}
			}
		}
	}
}