﻿using System.Collections.Generic;
using LD45.Game.Camera;
using LD45.Game.Core;
using LD45.Game.Menu;
using UnityEngine;

namespace LD45.Game.Utils
{
	public class AutoStarter : MonoBehaviour
	{
		[SerializeField]
		private bool isMenu;

		private void Start()
		{
			if (!GameObject.FindWithTag("MainCamera"))
			{
				Instantiate(Resources.Load("MainCamera"));

				LoadingScreen.Instance.Activate();
				LoadingScreen.Instance.FadeFromBlack(() =>
				{
					if (!isMenu)
					{
						var listeners = FindOfType<ILevelHandleInitialize>();
						foreach (var listener in listeners)
						{
							listener.HandleLevelInitialized();
						}
					}
				});
			}
		}

		public static List<T> FindOfType<T>() where T : class
		{
			var all = new List<T>();
			foreach (var monoBehaviour in FindObjectsOfType<MonoBehaviour>())
			{
				if (monoBehaviour is T type)
				{
					all.Add(type);
				}
			}

			return all;
		}
	}
}