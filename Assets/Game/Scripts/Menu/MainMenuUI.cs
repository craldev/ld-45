﻿using System.Linq;
using LD45.Game.Audio;
using LD45.Game.Core;
using LD45.Game.Settings;
using LD45.Game.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LD45.Game.Menu
{
	public class MainMenuUI : MonoBehaviour
	{
		[SerializeField]
		private UnityScene targetScene;

		[SerializeField]
		private CanvasGroup mainMenuCanvas;

		[SerializeField]
		private AudioClip music;

		private void Start()
		{
			AudioSystem.Instance.PlayMusic(music);
		}

		public void SettingsButton()
		{
			SettingsSystem.Instance.Open();
		}

		public void ActivatePlayButton()
		{
			mainMenuCanvas.interactable = false;
			LoadingScreen.Instance.Deactivate();
			AudioSystem.Instance.StopMusic();
			LoadingScreen.Instance.FadeToBlack(async () =>
			{
				await SceneManager.LoadSceneAsync(targetScene);
				var listeners = FindObjectsOfType<MonoBehaviour>().OfType<ILevelHandleInitialize>();
				foreach (var listener in listeners)
				{
					listener.HandleLevelInitialized();
				}
				LoadingScreen.Instance.FadeFromBlack();
			});
		}
	}
}