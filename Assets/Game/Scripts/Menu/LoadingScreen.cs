﻿using System;
using DUCK.Tween;
using DUCK.Tween.Easings;
using DUCK.Tween.Extensions;
using UnityEngine;

namespace LD45.Game.Menu
{
	public class LoadingScreen : MonoBehaviour
	{
		[SerializeField]
		private CanvasGroup canvasGroup;

		private UIFadeAnimation fadeAnimation;
		private bool isActive;

		public static LoadingScreen Instance { get; private set; }
		public static bool IsActive => Instance.isActive;

		private void Awake()
		{
			if (Instance != null)
			{
				throw new Exception("There should only be a single instance of 'LoadingSystem' active at one time.");
			}

			Instance = this;
		}

		public void Activate()
		{
			canvasGroup.alpha = 1;
			isActive = true;
		}

		public void Deactivate()
		{
			canvasGroup.alpha = 0;
			isActive = false;
		}

		public void FadeToBlack(Action onComplete = null, float duration = 1f)
		{
			if (isActive) return;

			fadeAnimation?.Abort();
			fadeAnimation = canvasGroup.Fade(canvasGroup.alpha, 1, duration, Ease.Quad.InOut);
			fadeAnimation.Play(onComplete);
			isActive = true;
		}

		public void FadeFromBlack(Action onComplete = null, float duration = 1f)
		{
			fadeAnimation?.Abort();
			fadeAnimation = canvasGroup.Fade(canvasGroup.alpha, 0, duration, Ease.Quad.InOut);
			fadeAnimation.Play(onComplete);
			isActive = false;
		}
	}
}