﻿using System.Linq;
using LD45.Game.Audio;
using LD45.Game.Core;
using LD45.Game.Gameplay;
using LD45.Game.Menu;
using LD45.Game.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LD45
{
	public class ElevatorDoor : PickableItem
	{
		[SerializeField]
		private Animator animator;

		[SerializeField]
		private AudioClip creak;

		[SerializeField]
		private UnityScene officeScene;

		private bool IsOpen = false;

		protected override async void OnUse()
		{
			animator.Play("Open");
			AudioSystem.Instance.PlaySound(creak);

			await Awaiters.Seconds(1f);
			AudioSystem.Instance.StopMusic();
			LoadingScreen.Instance.FadeToBlack(async () =>
			{
				await SceneManager.LoadSceneAsync(officeScene);
				var listeners = FindObjectsOfType<MonoBehaviour>().OfType<ILevelHandleInitialize>();
				foreach (var listener in listeners)
				{
					listener.HandleLevelInitialized();
				}

				LoadingScreen.Instance.FadeFromBlack();
			});
		}
	}
}