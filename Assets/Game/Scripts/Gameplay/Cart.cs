﻿using UnityEngine;

namespace LD45.Game.Gameplay
{
	public class Cart : MonoBehaviour
	{
		[SerializeField]
		private BoxCollider2D invisibleWall;

		public void RemoveInvisibleWall()
		{
			Destroy(invisibleWall);
		}
	}
}
