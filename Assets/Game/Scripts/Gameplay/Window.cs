using LD45.Game.Audio;
using LD45.Game.Menu;
using LD45.Game.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LD45.Game.Gameplay
{
	public class Window : PickableItem
	{
		[SerializeField]
		private SpriteRenderer window;

		[SerializeField]
		private Sprite brokenWindow;

		[SerializeField]
		private ParticleSystem glassBreak;

		[SerializeField]
		private AudioClip glassBreakSound;

		[SerializeField]
		private AudioClip fallSound;

		[SerializeField]
		private UnityScene endingScene;

		protected override async void OnUse()
		{
			glassBreak.Play();
			window.sprite = brokenWindow;
			AudioSystem.Instance.PlaySound(glassBreakSound);

			await Awaiters.Seconds(0.5f);
			AudioSystem.Instance.StopMusic();
			LoadingScreen.Instance.FadeToBlack(async () =>
			{
				await Awaiters.Seconds(0.4f);
				AudioSystem.Instance.PlaySound(fallSound);
				//Play Audio

				await Awaiters.Seconds(2f);

				await SceneManager.LoadSceneAsync(endingScene);
				LoadingScreen.Instance.FadeFromBlack();
			});
		}
	}
}