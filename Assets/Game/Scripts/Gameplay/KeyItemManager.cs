﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LD45.Game.Gameplay
{
	public enum KeyFlag
	{
		Null,
		Pass,
		Glasses,
		Cleaning,
		Order,
		Coffee,
		Robot,
		Keys,
		Hammer
	}

	public class KeyItemManager : MonoBehaviour
	{
		private static Dictionary<KeyFlag, bool> flags;

		public static event Action<KeyFlag> OnCollectedFlag;

		public static void Clear()
		{
			foreach (var flag in (KeyFlag[])Enum.GetValues(typeof(KeyFlag)))
			{
				flags[flag] = false;
				OnCollectedFlag?.Invoke(flag);
			}
		}

		public static bool Value(KeyFlag item)
		{
			if (flags == null)
			{
				flags = new Dictionary<KeyFlag, bool>();
				foreach (var flag in (KeyFlag[]) Enum.GetValues(typeof(KeyFlag)))
				{
					flags.Add(flag, flag == KeyFlag.Null);
				}
			}

			return flags[item];
		}
		public static void AddFlag(KeyFlag item)
		{
			if (flags == null)
			{
				flags = new Dictionary<KeyFlag, bool>();
				foreach (var flag in (KeyFlag[]) Enum.GetValues(typeof(KeyFlag)))
				{
					flags.Add(flag, flag == KeyFlag.Null);
				}
			}

			if (!flags.ContainsKey(item))
			{
				flags.Add(item, true);
			}
			flags[item] = true;
			OnCollectedFlag?.Invoke(item);
		}
	}
}