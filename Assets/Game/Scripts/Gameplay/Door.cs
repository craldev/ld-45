﻿using DUCK.Tween;
using DUCK.Tween.Extensions;
using LD45.Game.Audio;
using UnityEngine;

namespace LD45.Game.Gameplay
{
	public class Door : MonoBehaviour
	{
		[SerializeField]
		private Transform door;

		[SerializeField]
		private float duration;

		[SerializeField]
		private AudioClip open;

		private MoveAnimation moveDoorAnimation;

		private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.gameObject.layer == LayerMask.NameToLayer("PossessedCharacter"))
			{
				Open();
			}
		}

		private void Open()
		{
			AudioSystem.Instance.PlaySound(open);
			moveDoorAnimation?.Abort();
			moveDoorAnimation = door.MoveLocal(door.localPosition, Vector3.up * 2.364f, duration);
			moveDoorAnimation.Play();
		}
	}
}