using UnityEngine;

namespace LD45.Game.Gameplay
{
	public class CartTrigger : MonoBehaviour
	{
		private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.gameObject.layer == LayerMask.NameToLayer("Prop"))
			{
				var cart = other.GetComponent<Cart>();
				if (cart != null)
				{
					cart.RemoveInvisibleWall();
					KeyItemManager.AddFlag(KeyFlag.Cleaning);
					Destroy(gameObject);
				}
			}
		}
	}
}