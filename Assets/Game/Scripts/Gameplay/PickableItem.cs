using LD45.Game.Character;
using UnityEngine;

namespace LD45.Game.Gameplay
{
	public class PickableItem : MonoBehaviour
	{
		[SerializeField]
		private CharacterName canUseCharacter;

		[SerializeField]
		private KeyFlag canUseFlag;

		[SerializeField]
		private KeyFlag flag;

		[SerializeField]
		private SpriteRenderer sprite;

		[SerializeField]
		private SpriteRenderer ePrompt;

		private bool isPickable;
		private bool isActive = true;
		private InteractionHandler instigator;

		public virtual bool CanInteract(CharacterName name) => isActive && KeyItemManager.Value(canUseFlag) && (canUseCharacter == CharacterName.Null || canUseCharacter == name);

		private void Awake()
		{
			ClearPrompt();
		}

		protected virtual void Update()
		{
			if (isActive && isPickable && Input.GetButtonDown("Use") && CanInteract(instigator.Definition.Data.Character))
			{
				OnUse();
			}
		}

		protected virtual void OnUse()
		{
			KeyItemManager.AddFlag(flag);
			isActive = false;
			sprite.enabled = false;
			ClearPrompt();
		}

		public void Activate(InteractionHandler instigator)
		{
			this.instigator = instigator;
			isPickable = true;
			if (CanInteract(instigator.Definition.Data.Character))
			{
				ShowPrompt();
			}
		}

		public void ShowPrompt()
		{
			if (ePrompt != null)
			{
				ePrompt.enabled = true;
			}
		}

		public void ClearPrompt()
		{
			isPickable = false;
			if (ePrompt != null)
			{
				ePrompt.enabled = false;
			}
		}

	}
}