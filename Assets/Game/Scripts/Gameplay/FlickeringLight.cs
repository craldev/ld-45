﻿using UnityEngine;

namespace LD45.Game.Gameplay
{
	[RequireComponent(typeof(Animator))]
	public class FlickeringLight : MonoBehaviour
	{
		/// <summary>
		/// Indicates the range of time it should take between flickers
		/// </summary>
		private readonly (float min, float max) FlickerRate = (5f, 15f);

		/// <summary>
		/// Animator of this gameobject
		/// </summary>
		private Animator Anim;

		private void Awake()
		{
			Anim = GetComponent<Animator>();
		}

		/// <summary>
		/// Local timer for the flicker
		/// </summary>
		private float FlickerTimer = 0f;

		// Update is called once per frame
		private void Update()
		{
			FlickerTimer -= Time.deltaTime;
			if (FlickerTimer <= 0)
			{
				Anim.SetTrigger("Flicker");
				FlickerTimer = Random.Range(FlickerRate.min, FlickerRate.max);
			}
		}
	}
}