﻿using Cinemachine;
using LD45.Game.Character;
using LD45.Game.Core;
using LD45.Game.Event;
using UnityEngine;

namespace LD45.Game.Camera
{
	public class CameraController : MonoBehaviour, ILevelHandleInitialize
	{
		[SerializeField]
		private Animator cameraStateMachine;

		[SerializeField]
		private CinemachineBrain brain;

		[SerializeField]
		private CinemachineVirtualCamera possessedCamera;

		[SerializeField]
		private CinemachineVirtualCamera ghostCamera;

		[SerializeField]
		private CinemachineVirtualCamera eventCamera;

		[SerializeField]
		private CinemachineStateDrivenCamera stateDrivenCamera;

		private GameService gameService;

		private void Awake()
		{
			DontDestroyOnLoad(gameObject);
			cameraStateMachine.transform.SetParent(null);
			cameraStateMachine.transform.position = Vector3.zero;
			EventSystem.OnEventFinished += HandlePossess;
			EventSystem.OnEventStarted += HandleEvent;
		}

		private void HandleGameplay()
		{
			cameraStateMachine.Play("Ghost");
		}

		private void HandleEvent()
		{
			eventCamera.transform.position = ghostCamera.transform.position;
			cameraStateMachine.Play("Event");
		}

		public void HandleLevelInitialized()
		{
			gameService = FindObjectOfType<GameService>();
			gameService.StartingPlayer.OnPossess += gameObject => HandlePossess();
			gameService.StartingPlayer.OnUnPossess += gameObject => HandleGameplay();
			UpdateCamera();
		}

		private async void HandlePossess()
		{
			await Awaiters.While(() => stateDrivenCamera.IsBlending);
			possessedCamera.Follow = gameService.CurrentPlayer.transform;
			cameraStateMachine.Play("Possess");
		}

		private async void UpdateCamera()
		{
			ghostCamera.Follow = gameService.CurrentPlayer.transform;
			EventFocusPoint.Instance.MoveToLocation(gameService.CurrentPlayer.transform);
			brain.enabled = false;
			ghostCamera.enabled = false;
			possessedCamera.enabled = false;
			await Awaiters.NextFrame;
			ghostCamera.transform.position = gameService.CurrentPlayer.transform.position;
			possessedCamera.transform.position = gameService.CurrentPlayer.transform.position;
			await Awaiters.NextFrame;
			brain.enabled = true;
			ghostCamera.enabled = true;
			possessedCamera.enabled = true;
			HandleGameplay();
		}
	}

}